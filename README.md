# JavaScript

## How JavaScript works
- [An overview of the engine, the runtime, and the call stack](https://blog.sessionstack.com/how-does-javascript-actually-work-part-1-b0bacc073cf)__[[RUS]](https://habr.com/company/ruvds/blog/337042/)__
- [Inside the V8 engine + 5 tips on how to write optimized code](https://blog.sessionstack.com/how-javascript-works-inside-the-v8-engine-5-tips-on-how-to-write-optimized-code-ac089e62b12e)__[[RUS]](https://habr.com/company/ruvds/blog/337460/)__
- [Memory management + how to handle 4 common memory leaks](https://blog.sessionstack.com/how-javascript-works-memory-management-how-to-handle-4-common-memory-leaks-3f28b94cfbec)__[[RUS]](https://habr.com/company/ruvds/blog/338150/)__
- [Event loop and the rise of Async programming + 5 ways to better coding with async/await](https://blog.sessionstack.com/how-javascript-works-event-loop-and-the-rise-of-async-programming-5-ways-to-better-coding-with-2f077c4438b5)__[[RUS]](https://habr.com/company/ruvds/blog/340508/)__
- [Deep dive into WebSockets and HTTP/2 with SSE + how to pick the right path](https://blog.sessionstack.com/how-javascript-works-deep-dive-into-websockets-and-http-2-with-sse-how-to-pick-the-right-path-584e6b8e3bf7)__[[RUS]](https://habr.com/company/ruvds/blog/342346/)__
- [A comparison with WebAssembly + why in certain cases it’s better to use it over JavaScript](https://blog.sessionstack.com/how-javascript-works-a-comparison-with-webassembly-why-in-certain-cases-its-better-to-use-it-d80945172d79)__[[RUS]](https://habr.com/company/ruvds/blog/343568/)__
- [The building blocks of Web Workers + 5 cases when you should use them](https://blog.sessionstack.com/how-javascript-works-the-building-blocks-of-web-workers-5-cases-when-you-should-use-them-a547c0757f6a)__[[RUS]](https://habr.com/company/ruvds/blog/348424/)__
- [Service Workers, their lifecycle and use cases](https://blog.sessionstack.com/how-javascript-works-service-workers-their-life-cycle-and-use-cases-52b19ad98b58)__[[RUS]](https://habr.com/company/ruvds/blog/349858/)__
- [How JavaScript works: the mechanics of Web Push Notifications](https://blog.sessionstack.com/how-javascript-works-the-mechanics-of-web-push-notifications-290176c5c55d)__[[RUS]](https://habr.com/company/ruvds/blog/350486/)__
- [Tracking changes in the DOM using MutationObserver](https://blog.sessionstack.com/how-javascript-works-tracking-changes-in-the-dom-using-mutationobserver-86adc7446401)__[[RUS]](https://habr.com/company/ruvds/blog/351256/)__
- [The rendering engine and tips to optimize its performance](https://blog.sessionstack.com/how-javascript-works-the-rendering-engine-and-tips-to-optimize-its-performance-7b95553baeda)__[[RUS]](https://habr.com/company/ruvds/blog/351802/)__
- [Inside the Networking Layer + How to Optimize Its Performance and Security](https://blog.sessionstack.com/how-javascript-works-inside-the-networking-layer-how-to-optimize-its-performance-and-security-f71b7414d34c)__[[RUS]](https://habr.com/company/ruvds/blog/354070/)__
- [Under the hood of CSS and JS animations + how to optimize their performance](https://blog.sessionstack.com/how-javascript-works-under-the-hood-of-css-and-js-animations-how-to-optimize-their-performance-db0e79586216)__[[RUS]](https://habr.com/company/ruvds/blog/354438/)__
- [How JavaScript works: Parsing, Abstract Syntax Trees (ASTs) + 5 tips on how to minimize parse time](https://blog.sessionstack.com/how-javascript-works-parsing-abstract-syntax-trees-asts-5-tips-on-how-to-minimize-parse-time-abfcf7e8a0c8)__[[RUS]](https://habr.com/company/ruvds/blog/415269/)__
- [How JavaScript works: The internals of classes and inheritance + transpiling in Babel and TypeScript](https://blog.sessionstack.com/how-javascript-works-the-internals-of-classes-and-inheritance-transpiling-in-babel-and-113612cdc220)__[[RUS]](https://habr.com/company/ruvds/blog/415377/)__
- [How JavaScript works: Storage engines + how to choose the proper storage API](https://blog.sessionstack.com/how-javascript-works-the-internals-of-shadow-dom-how-to-build-self-contained-components-244331c4de6e)__[[RUS]](https://habr.com/company/ruvds/blog/415505/)__
- [How JavaScript works: the internals of Shadow DOM + how to build self-contained components](https://blog.sessionstack.com/how-javascript-works-the-internals-of-shadow-dom-how-to-build-self-contained-components-244331c4de6e)__[[RUS]](https://habr.com/company/ruvds/blog/415881/)__
- [How JavaScript works: WebRTC and the mechanics of peer to peer networking](https://blog.sessionstack.com/how-javascript-works-webrtc-and-the-mechanics-of-peer-to-peer-connectivity-87cc56c1d0ab)__[[RUS]](https://habr.com/company/ruvds/blog/416821/)__
- [How JavaScript works: Under the hood of custom elements + Best practices on building reusable components](https://blog.sessionstack.com/how-javascript-works-under-the-hood-of-custom-elements-best-practices-on-building-reusable-e118e888de0c)__[[RUS]](https://habr.com/company/ruvds/blog/419831/)__

- [Jake Archibald: In The Loop](https://www.youtube.com/watch?v=cCOL7MC4Pl0)

- [Writing a JavaScript Framework - Project Structuring](https://blog.risingstack.com/writing-a-javascript-framework-project-structuring/)

## NodeJs
- [The Node.js Event Loop, Timers, and process.nextTick()](https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/)
- [You Don't Know Node - ForwardJS San Francisco](https://www.youtube.com/watch?v=oPo4EQmkjvY) -> [Getting Started with Node.js - Full Tutorial](https://www.youtube.com/watch?v=gG3pytAY2MY)
- [You don’t know Node](https://medium.com/@samerbuna/you-dont-know-node-6515a658a1ed)
- [What you should know to really understand the Node.js Event Loop](https://medium.com/the-node-js-collection/what-you-should-know-to-really-understand-the-node-js-event-loop-and-its-metrics-c4907b19da4c)

- [Node.js Streams: Everything you need to know](https://medium.freecodecamp.org/node-js-streams-everything-you-need-to-know-c9141306be93)
- [Node.js Child Processes: Everything you need to know](https://medium.freecodecamp.org/node-js-child-processes-everything-you-need-to-know-e69498fe970a)
- [Scaling Node.js Applications](https://medium.freecodecamp.org/scaling-node-js-applications-8492bd8afadc)
- [Вы наверное шутите, мистер Дал, или почему Node.js — это венец эволюции веб-серверов](https://habr.com/post/108241/)
- [Node.js Child Processes: Everything you need to know](https://medium.freecodecamp.org/node-js-child-processes-everything-you-need-to-know-e69498fe970a)
- [Node.js Best Practices](https://github.com/i0natan/nodebestpractices)
- [Advanced](https://blog.risingstack.com/nodejs-at-scale-npm-best-practices/)
- [The Best Node.js & Microservices Articles we Ever Wrote](https://blog.risingstack.com/top-nodejs-microservices-articles-risingstack/#top11mostreadpostsontherisingstackblog)

## Common
- [JavaScript VM internals, EventLoop, Async and ScopeChains](https://www.youtube.com/watch?v=QyUFheng6J0&feature=youtu.be)
- [In The Loop](https://www.youtube.com/watch?v=cCOL7MC4Pl0)
- [Рендеринг WEB-страницы: что об этом должен знать front-end разработчик](https://habr.com/post/224187/)

- [JavaScript type coercion explained](https://medium.freecodecamp.org/js-type-coercion-explained-27ba3d9a2839)__[[RUS]](https://habr.com/company/ruvds/blog/347866/)__
- [An Introduction to Regular Expressions (Regex) In JavaScript](https://codeburst.io/an-introduction-to-regular-expressions-regex-in-javascript-1d3559e7ac9a)

- [Using web workers for safe, concurrent JavaScript](https://blog.logrocket.com/using-webworkers-for-safe-concurrent-javascript-3f33da4eb0b2)__[[RUS]](https://habr.com/company/ruvds/blog/352828/)__
- [Object Composition in Javascript](https://medium.com/code-monkey/object-composition-in-javascript-2f9b9077b5e6)
- [javascript-algorithms](https://github.com/trekhleb/javascript-algorithms)

- [Асинхронное программирование: концепция, реализация, примеры](https://proglib.io/p/asynchrony/)

- [You Don't Know JS Yet](https://github.com/getify/You-Dont-Know-JS)


## Code
- [Clean code in JavaScript](https://github.com/magic-top-hat/clean-code-javascript)
- [Рефакторринг + Шаборны](https://refactoring.guru/ru)
- [The System Design Primer](https://github.com/donnemartin/system-design-primer)
- [The Art of Command Line](https://github.com/jlevy/the-art-of-command-line)
- [Clean code javascript](https://github.com/ryanmcdermott/clean-code-javascript)
- [Design Patterns For Humans](https://github.com/kamranahmedse/design-patterns-for-humans#structural-design-patterns)
- [SOLID](https://samueleresca.net/solid-principles-using-typescript/)

## TypeScript
- [React+TypeScript Cheatsheets](https://github.com/typescript-cheatsheets/react-typescript-cheatsheet)
- [Modern Typescript with Examples Cheat Sheet](https://github.com/David-Else/modern-typescript-with-examples-cheat-sheet)

## Docs
- [Web Api](https://developer.mozilla.org/en-US/docs/Web/API)
- [Javascript Info](https://javascript.info/)

## Links
- [Build your own x](https://github.com/danistefanovic/build-your-own-x)
- [Every Programmer Should Know](https://github.com/mtdvio/every-programmer-should-know)
- [Git Tips](https://github.com/Imangazaliev/git-tips)
- [Developer Roadmap](https://github.com/kamranahmedse/developer-roadmap)

### Job
- [The Best Resources to Ace your Full Stack JavaScript Interview](https://medium.freecodecamp.org/5-top-sites-for-javascript-interview-preparation-71b48e9a6c8a)
- [The Definitive JavaScript Handbook for your next developer interview](https://medium.freecodecamp.org/the-definitive-javascript-handbook-for-a-developer-interview-44ffc6aeb54e)
- [10 JavaScript concepts you need to know for interviews](https://codeburst.io/10-javascript-concepts-you-need-to-know-for-interviews-136df65ecce)
- [JavaScript Questions](https://github.com/lydiahallie/javascript-questions)

